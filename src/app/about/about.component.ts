import { Component, OnInit } from '@angular/core';
import { Content } from '../models/content.model';
import {HttpClient} from '@angular/common/http';
import {JsonResponse} from "../models/jsonResponse.model";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent  {

  private pageTitle = 'About Us';
  private content: string;

  constructor(private http: HttpClient) {
    http.get<JsonResponse>('/assets/catnip.json').subscribe(res => {
      this.content = res.AboutUs;
    });
  }




}
