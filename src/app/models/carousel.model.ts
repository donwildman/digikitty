export interface Carousel {
  url: string;
  title: string;
}
