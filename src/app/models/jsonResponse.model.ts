import {Carousel} from './carousel.model';

export interface JsonResponse {
  AboutUs: string;
  Carousel: Carousel[];
}
