import {AfterContentInit, Component, OnInit, AfterViewInit, ElementRef, ViewChild} from '@angular/core';
import * as Immutable from 'immutable';

import { Carousel } from '../models/carousel.model';

import {HttpClient} from '@angular/common/http';
import { JsonResponse } from '../models/jsonResponse.model';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private pageTitle = 'Home';
  private carouselArray: Carousel[] = [];
  carousel: Carousel = {url: '', title: ''};


  public myInterval: number = 3000;
  public activeSlideIndex: number = 0;
  public noWrapSlides:boolean = false;

  public images: Carousel[] = [];

  private hasImages = false;


  constructor(private http: HttpClient) {
    http.get<JsonResponse>('/assets/catnip.json').subscribe(res => {
      // const body = res.json();
      res.Carousel.forEach( (item: Carousel) => {
        console.log(item);
        this.images.push(item);
      });
      // console.log('content: ' + this.carouselArray[0].url);
      this.hasImages = true;
    });
  }

  ngOnInit() {

  }

  onAddTile() {

    // this.carousel.title = title;
    const _url: string = 'http://placekitten.com/' + this.getRandomInt(700,1000) + '/' + this.getRandomInt(500,600);
    this.carousel.url = _url;
    console.log(this.carousel);
    this.images.push({ url: this.carousel.url, title: this.carousel.title});
    // console.log(this.carouselArray);
    // this.images = Immutable.List(this.carouselArray);
    // this.carousel.title = null;
  }

  private getRandomInt(min, max) {
    const rnd = Math.floor(Math.random() * (max - min + 1)) + min;
    return Math.round(rnd / 10) * 10;
  }

}
